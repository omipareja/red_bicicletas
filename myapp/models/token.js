const mongoose = require('mongoose');

const Schema = mongoose.Schema; 

const TokenSchema = new Schema ({
    _userId: {type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Usuario'}, // se pasa el id del usuario referencia y tenemos acesso a los datos del usuario
    token: {type: String, required: true},
    createdAt: {type: Date ,required: true, default: Date.now, expires: 4300} // se crea la fecha de expiracion se elimina este token

});

module.exports = mongoose.model('Token',TokenSchema);