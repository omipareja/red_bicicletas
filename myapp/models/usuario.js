var mongoose = require('mongoose');
var Reserva = require('./reserva');
const uniqueValidator = require('mongoose-unique-validator');
const bcrypt = require('bcrypt');
const crypto = require('crypto');

const saltRounds = 10;

const Token = require('../models/token');
const mailer = require('../mailer/mailer');

var Schema = mongoose.Schema;



const validateEmail = function(email){
    const re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
    return re.test(email);

}    
var usuarioSchema = new Schema({
    nombre: {
        type: String, 
        trin: true, // si hay espacios vacios se eliminan
        required: [true,'El nombre es obligatorio']
    },
    email: {
        type:String,
        trin: true,
        required: [true, 'El email es obligatorio'], // error si no se escribe
        lowercase: true, // pone todo en minuscula
        unique: true,
        validate: [validateEmail, 'por favor ingrese un email valido'],
        match: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/
    },

    password:{
        type: String,
        required: [true,'El password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado:{
        type: Boolean,
        default: false
    }

});

usuarioSchema.plugin(uniqueValidator, {message: ' El {PATH} ya existe con otro usuario'});

usuarioSchema.pre('save',function(next){// cada que se hace un save se va ejecutar esta fucniom 
    if (this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function (password){
    return bcrypt.compareSync(password, this.password); // se compara la inscirptacion del mismo password
}

usuarioSchema.methods.reservar = function (biciId, desde , hasta, cb){
    var reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta});
    console.log(reserva);
    reserva.save(cb);
}


usuarioSchema.methods.enviar_email_bienvenida = function(cb){
    const token = new Token({_userId: this.id, token : crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function (err) {
        if(err) {return console.log(err.message);}

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'verificacion de cuenta',
            text: 'Hola, \n\n' + 'Cuenta buena XD haga click aqu: \n ' + 'http://localhost:5000' + '\/token/confirmation\/' + token.token + ' .\n' // revisar
        };

        mailer.sendMail(mailOptions, function(err){
            if(err) { return console.log(err.message); }

            console.log('Verificacion enviada a ' + email_destination + '.');
        });
    });
}



usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition,callback){
    const self = this;
    console.log(condition);
    self.findOne({
        sort: [
            {'facebookId': condition.id},
            {'email': condition.emails[0].value}    
        ]

        },(err,result)=>{
            if(result){
                callback(err,result);
            }else{
                console.log('---------- condition ------------');
                console.log(condition);
                let values = {};
                values.facebookId = condition.id;
                values.email = condition.emails[0].value;
                values.nombre = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = crypto.randomBytes(16).toString('hex');//condition._json.sub || condition.emails[0].value+'SIN PASSWORD';
                console.log('---------------- values ---------------');
                console.log(values);
                self.create(values,(err,result)=>{
                    if(err)
                        console.log(err);
                    return callback(err,result);
                });
            }
        }
    );  
};


usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition,callback){
    const self = this;
    console.log(condition);
    self.findOne({
        sort: [
            {'googleId': condition.id},
            {'email': condition.emails[0].value}    
        ]

        },(err,result)=>{
            if(result){
                callback(err,result);
            }else{
                console.log('---------- condition ------------');
                console.log(condition);
                let values = {};
                values.googleId = condition.id;
                values.email = condition.emails[0].value;
                values.nombre = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = condition._json.sub || condition.emails[0].value+'SIN PASSWORD';  // en caso de error condition._json.sub
                console.log('---------------- values ---------------');
                console.log(values);
                self.create(values,(err,result)=>{
                    if(err)
                        console.log(err);
                    return callback(err,result);
                });
            }
        }
    );  
};

usuarioSchema.methods.resetPassword = function(cb){
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function (err){

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Reseteo de password',
            text: 'Hola, \n\n' + 'Por favor haga click aca para resetear password \n' + 'http://localhost:5000'+ '\/resetPassword\/' + token.token + ' ./n'
        };

        mailer.sendMail(mailOptions, function(err){
            if(err) { return cb(err); }
            console.log('Verificacion enviada a ' + email_destination + '.');
        });

        cb(null);

    });
}

module.exports = mongoose.model('Usuario',usuarioSchema);